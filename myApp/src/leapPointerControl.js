var wsio;

var mainCanvas = document.getElementById("leapPosition");
    mainCanvas.width = document.getElementsByTagName('body')[0].clientWidth;
    mainCanvas.height = window.innerHeight-25;
var mainContext = mainCanvas.getContext("2d");

var canvasWidth = mainCanvas.width;
var canvasHeight = mainCanvas.height;

var x = location.origin;

var radius = 10; // radius of the control point at help screen
var testImageRadius = 0.1;

//console.log(x);

// setup parametrs

var WALL_WIDTH = 9600;
var WALL_HEIGHT = 4320;

// app states

var FREE_CURSOR = "#0000FF";
var HOLD_CURSOR = "#FF0000";
var ZOOM_CURSOR = "#FFFF00";
var KEYTAP_CURSOR = "#00FF00";

var VERTICAL = 1;
var HORIZONTAL = 2;

var STATE = FREE_CURSOR;
var orientation = VERTICAL;

var lastPosBeforeZoom;


// --------------------------------------------------------
// Configuration of WS comunications
// --------------------------------------------------------
if(x == 'file://'){
    wsio = null;
    interactor = null;
} else {
      wsio = new WebsocketIO();
      console.log("Connected to server: ", window.location.origin);
      console.log(wsio.id);

      wsio.open(function() {
        console.log("Websocket opened");

        setupListeners();
        wsio.emit('requestAvailableApplications');
          
        var clientDescription = {
          //clientType: "display",
          clientType: "sageUI",
          requests: {
            config: true,
            version: true,
            time: true,
            console: false
          }
        };
        wsio.emit('addClient', clientDescription);

      });
}
// Explicitely close web socket when web browser is closed
window.onbeforeunload = function() {
  wsio.emit('stopSagePointer');
  if (wsio !== undefined) {
    wsio.close();
  }
};

function setupListeners() {
	wsio.on('initialize', function(data) {
		//interactor.setInteractionId(data.UID);
		pointerDown = false;
		pointerX    = 0;
		pointerY    = 0;

		var sage2UI = document.getElementById('sage2UICanvas');

	});

	// Open a popup on message sent from server
	wsio.on('errorMessage', function(data) {
		showSAGE2Message(data);
	});

	wsio.on('setupDisplayConfiguration', function(config) {

	});

	wsio.on('setSystemTime', function(config) {

	});
    
	wsio.on('updateItemOrder', function(config) {

	});    

	// Server sends the SAGE2 version
	wsio.on('setupSAGE2Version', function(data) {
		sage2Version = data;
		console.log('SAGE2: version', data.base, data.branch, data.commit, data.date);
	});

    wsio.on('availableApplications', function(data) {
        console.log(data);
    });    
    
}


// --------------------------------------------------------
// Wsio functions
// --------------------------------------------------------
function sendPosition( pos ){
    var data = {};
    data.pointerX = pos[0];
    data.pointerY = pos[1];
    //wsio.emit('pointerPosition', data );
    send('pointerPosition', data);
    
}

function send( type, data ){
    if(wsio == null){
        console.log("Send: "+type);
    } else {
        if(data == null)
            wsio.emit(type);
        else
            wsio.emit(type, data);
    }
}

function updatePointer(color){
    send('stopSagePointer', null);
    var pointerData = {};
          pointerData.label = "Leap";
          pointerData.color = color;
          send('startSagePointer', pointerData);
    
}


// --------------------------------------------------------
// Support screen drawing
// --------------------------------------------------------

function draw(circlePos, squarePos, zoom){
    mainContext.clearRect(0, 0, canvasWidth, canvasHeight);
    
    // color in the background
    mainContext.fillStyle = "#EEEEEE";
    mainContext.fillRect(0, 0, canvasWidth, canvasHeight);
    
    if(testImageRadius < 0.01)
        testImageRadius = 0.01;
    
        base_image = new Image();
        //base_image.src = 'img/eso1031b.jpg';
        mainContext.drawImage(base_image, (canvasWidth/2-(1920*testImageRadius)/2), (canvasHeight/2-(1200*testImageRadius)/2), 1920*testImageRadius, 1200*testImageRadius);    
        
    
    mainContext.beginPath();
    
    if(circlePos)
        drawCircle(circlePos);
    if(squarePos)
        drawSquare(squarePos);
    
    mainContext.closePath();
    // color in the shapes
    mainContext.fillStyle = STATE;
    mainContext.fill();    
    

    
}

function drawCircle(pos) {
    if(radius < 1)
        radius = 1;
    mainContext.arc(pos[0], pos[1], radius, 0, Math.PI * 2, false);
}

function drawSquare(pos) {
    if(radius < 1)
        radius = 1;    
    mainContext.rect(pos[0], pos[1], radius, radius);    
}


// --------------------------------------------------------
// Support Calculations
// --------------------------------------------------------

function map(nr, in_min, in_max, out_min, out_max) {
  var res = Math.floor((nr - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
  if(res > out_max)
      return out_max;
  else if( res < out_min)
      return out_min;
  else 
      return res;
}        


/**
    The function that translates the hand position coordinates from Leap to pointer coordinates at the screen.
    @param position Coordinates received from the Leap
    @param windowSize The size of the screen/window that the coordinates hsould be translated to
    @return screenPos position of the Pointer at the screen
*/
function leapPosToScreenPos(pos, windowSize){
    var position = round(pos, 1);
    var screenPos;
    switch(orientation){
        case VERTICAL:
            screenPos = [ map(position[0], -200, 200, 0, windowSize[0]), 
                              windowSize[1]-map(position[1], 130, 330, 0, windowSize[1])
                            ];
            break;
        case HORIZONTAL:
            screenPos = [ map(position[0], -200, 200, 0, windowSize[0]), 
                              map(position[2], -200, 200, 0, windowSize[1])
                            ];    
            break;                        
    }              
    return screenPos;
}

function speed(velocityVec){
    
    switch(orientation){
        case VERTICAL:
            return ( Math.abs(velocityVec[0]) > 20 || Math.abs(velocityVec[1]) > 10 );
            break;
        case HORIZONTAL:
            return ( Math.abs(velocityVec[0]) > 20 || Math.abs(velocityVec[2]) > 20 );
            break;                        
    }     
}

/**
    The function translates the Leap data (hand position) into the zoom ratio to be used for zooming in apps
    @param position Coordinates received from the Leap
    @param range The range in which the resulting zoom should be
    @return zoomRatio
*/
function leapPosToZoom(pos, self){
    var position = round(pos, 1);
    switch(orientation){
        case VERTICAL:
            if( position[2] > 50 )
                self.emit('zoomOUT');
            if( position[2] < (-50) )                
                self.emit('zoomIN');
                
        break;
        case HORIZONTAL:
        default:
            
    }  
    return 0;
}

function handsDistanceToZoom( current, previous ){
    //var current = round(cur, 3);
    //var previous = round(prev, 3);
    if(  (Math.abs( current[0][0] - current[1][0] ) - Math.abs( previous[0][0] - previous[1][0] )) > 200 )
        return 20;
    //else if( (Math.abs( current[0][0] - current[1][0] ) - Math.abs( previous[0][0] - previous[1][0] ))  )
    else
        return 0.2;
}


function handToPointerPosition(){
    
}

function switchOrientation(){
    switch(orientation){
        case VERTICAL:
            orientation = HORIZONTAL;
            break;
        case HORIZONTAL:
            orientation = VERTICAL;
            break;
        default:
            orientation = VERTICAL;
    }
}

function round(vector, x){
    if(!x){
        return [Math.ceil(vector[0]*10)/10, Math.ceil(vector[1]*10)/10, Math.ceil(vector[2]*10)/10 ];    
    } else {
        return [Math.ceil(vector[0]*Math.pow(10,x))/Math.pow(10,x), Math.ceil(vector[1]*Math.pow(10,x))/Math.pow(10,x), Math.ceil(vector[2]*Math.pow(10,x))/Math.pow(10,x) ];
    }
    
}

function evalkeys(event){
    console.log(event);
    switch(event.which){
        case 116: 
            switchOrientation();
            break;
        case 113:
                send('keyDown', {code: 16});
                send('keyPress', {code: 9, character: String.fromCharCode(9)});
                send('keyUp', {code: 16});            
            break;
    }
}

// --------------------------------------------------------
// Leap Data processing functions
// --------------------------------------------------------

function grab(strength, self){
    switch(STATE){
        case FREE_CURSOR:
            if(strength > 0.6){
                self.emit('pointerGrab');
                STATE = HOLD_CURSOR;
            }
            break;
        case HOLD_CURSOR:
            if(strength < 0.6){
                self.emit('pointerRelease');
                STATE = FREE_CURSOR;
            }
            break;
        case ZOOM_CURSOR:
            
            break;
    }
}

function evaluateHandPositions(leftHand, rightHand, self){
    var supportPointerPositions = {};
    switch(STATE){
        case FREE_CURSOR:
            if(rightHand && rightHand.grabStrength > 0.6){
                STATE = HOLD_CURSOR;
                self.emit('pointerGrab');
            } else if(rightHand){
//                supportPointerPositions.rightHand = leapPosToScreenPos(rightHand.palmPosition, [mainCanvas.width, mainCanvas.height]);
                if( speed(rightHand.palmVelocity) && rightHand.grabStrength < 0.2 )
                    self.emit('pointerMove' ,leapPosToScreenPos(rightHand.palmPosition, [WALL_WIDTH, WALL_HEIGHT]) );
            } else {
                self.emit('pointerLost');
            }
            break;
        case HOLD_CURSOR:
            if(rightHand && rightHand.grabStrength < 0.5){
                STATE = FREE_CURSOR;
                self.emit('pointerRelease');
            } else if(leftHand && leftHand.grabStrength > 0.6) {
                STATE = ZOOM_CURSOR;  
                lastPosBeforeZoom = leftHand.palmPosition;
                updatePointer(STATE);
                self.emit('zoomStart');
            } else if(rightHand){
//                supportPointerPositions.rightHand = leapPosToScreenPos(rightHand.palmPosition, [mainCanvas.width, mainCanvas.height]);
                if( speed(rightHand.palmVelocity) && rightHand.grabStrength > 0.8 )
                    self.emit('pointerMove' ,leapPosToScreenPos(rightHand.palmPosition, [WALL_WIDTH, WALL_HEIGHT]) );                
            } else {
                self.emit('pointerLost');
            }
            break;
        case ZOOM_CURSOR:
            console.log("zoom_state");
            if( !rightHand || !leftHand || rightHand.grabStrength < 0.5 || leftHand.grabStrength < 0.6 ){
                self.emit('zoomEnd');
                STATE = FREE_CURSOR;                 
            }
            else if( rightHand && leftHand && rightHand.grabStrength > 0.6 && leftHand.grabStrength > 0.6){
                var delta = leapPosToZoom(leftHand.palmPosition, self);
            } else {
                self.emit('zoomEnd');
                STATE = FREE_CURSOR;                
            }
            break;
        default:
            STATE = FREE_CURSOR;
    }   
    
    return supportPointerPositions;
}

// --------------------------------------------------------
// The main Leap Controller Loop with event listeners that do the interaction magic
// --------------------------------------------------------
var controller = Leap.loop({
    enableGestures: true
//}, function (frame){
})
.use('screenPosition', {positioning: 'absolute', scale: 1})
.use('handEntry')
.on('handFound', 
        function(hand){
            console.log( hand.type+' hand found at '+ round(hand.palmPosition, 1), hand );
          var pointerData = {};
//          pointerData.sourceType = "Pointer";
          pointerData.label = "Leap";
          pointerData.color = STATE;
          if(hand.type == 'right')
                send('startSagePointer', pointerData);
        }
   )
.on('handLost',
        function(hand){
            console.log( hand.type+' hand lost', hand );
            //wsio.emit('stopSagePointer');
            if(hand.type == 'right')
                send('stopSagePointer', null);
        }
   )
.on('frame', function(frame){
    
     
    var supportPointerPositions = {};
    // get both hands with distinction which is left and right
    for(var i = 0; i < frame.hands.length; i++){
        switch(frame.hands[i].type){
            case "right":
                var rightHand = frame.hands[i];
                break;
            case "left":
                var leftHand = frame.hands[i];
                break;
        }
    }
        
    evaluateHandPositions(leftHand, rightHand, this);
    
    if(rightHand)
        supportPointerPositions.rightHand = leapPosToScreenPos( rightHand.palmPosition, [mainCanvas.width, mainCanvas.height]);
    if(leftHand)
        supportPointerPositions.leftHand = leapPosToScreenPos( leftHand.palmPosition, [mainCanvas.width, mainCanvas.height]);
    
    draw(supportPointerPositions.rightHand, supportPointerPositions.leftHand);    

})
.on('pointerMove', function(pointerPositions){
    //console.log("pointerMove");
    sendPosition(pointerPositions);
})
.on('pointerGrab', function(){  
  console.log("pointerGrab"); 
  updatePointer(STATE);
  var dataPress = {};
  dataPress.button = "left";    
  send('pointerPress', dataPress);
})
.on('pointerRelease', function(){   
  console.log("pointerRelease");  
  var dataPress = {};
  dataPress.button = "left";    
  send('pointerRelease', dataPress);    
  updatePointer(STATE);
})
.on('zoomStart', function(){
    send('pointerScrollStart', null);
})
.on('zoomEnd', function(){
    send('pointerScrollEnd', null);    
})
.on('zoomIN', function(){
    var data = {};
    console.log('zoomIN');
    data.wheelDelta = 20;//map(delta, -50, 50, -1, 1 );      
    send('pointerScroll', data);    
})
.on('zoomOUT', function(){
    console.log('zoomOUT');
    var data = {};
    data.wheelDelta = -20;//map(delta, -50, 50, -1, 1 );  
    send('pointerScroll', data);
})
.on('gesture', function(gesture){
    switch (gesture.type){
//      case "circle":
//          //console.log("Circle Gesture");
//          break;
      case "keyTap":
          console.log("Key Tap Gesture");
          var dataPress = {};
          dataPress.button = "left";              
          send('pointerPress', dataPress);
          send('pointerRelease', dataPress);     
          break;
//      case "screenTap":
//          console.log("Screen Tap Gesture");
//          break;
//      case "swipe":
//          console.log("Swipe Gesture");
//          break;
    }    
})
;